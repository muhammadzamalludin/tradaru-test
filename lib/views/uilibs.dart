import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color baseColor = Colors.white70;
Color primaryColor = Color(0xFF4E55AF);
Color secondaryColor = Color(0xFFA4E2FE);

TextStyle title = GoogleFonts.roboto()
    .copyWith(color: Colors.black, fontWeight: FontWeight.w800);

TextStyle subTitle = GoogleFonts.roboto()
    .copyWith(fontSize: 12,color: Colors.black,fontWeight: FontWeight.bold);

TextStyle productTitle = GoogleFonts.roboto()
    .copyWith(color: primaryColor, fontWeight: FontWeight.bold);

TextStyle productSubTitle = GoogleFonts.roboto()
    .copyWith(color: Colors.blue, fontWeight: FontWeight.bold);

TextStyle blueTextCategory = GoogleFonts.roboto().copyWith(fontSize: 10,
    color: primaryColor, fontWeight: FontWeight.bold);