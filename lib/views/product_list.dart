import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:money2/money2.dart';
import 'package:tradaru_test/models/product.dart';
import 'package:tradaru_test/views/product_detail.dart';
import 'package:tradaru_test/views/uilibs.dart';

class ProductList extends StatefulWidget {
  @override
  _ProductListState createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: baseColor,
      appBar: header(),
      body: body(),
    );
  }

  AppBar header() {
    return AppBar(
      backgroundColor: baseColor,
      elevation: 0,
      centerTitle: true,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "X",
            style: GoogleFonts.libreBaskerville()
                .copyWith(color: primaryColor, fontWeight: FontWeight.bold),
          ),
          Text(
            "E",
            style: GoogleFonts.libreBaskerville()
                .copyWith(color: secondaryColor, fontWeight: FontWeight.bold),
          )
        ],
      ),
      leading: Icon(
        Icons.menu,
        color: Colors.black,
      ),
      actions: <Widget>[
        Padding(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Icon(Icons.search, color: Colors.black))
      ],
    );
  }

  // Build body
  Widget body() {
    return Padding(
      padding: EdgeInsets.all(15),
      child: Column(
        children: <Widget>[
          // Build header
          Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Our Product",
                    style: GoogleFonts.roboto()
                        .copyWith(fontSize: 20,color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                  Row(
                    children: [
                      Text(
                        "Sort By",
                        style: title,
                      ),
                      Icon(Icons.keyboard_arrow_down)
                    ],
                  )

                ],
              ),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  category('Sneakers', 10,TextStyle(
                    color: Colors.blue[800]
                  )),
                  category('Watch', 5, subTitle),
                  category('Backpack', 5, subTitle),
                ],
              )
            ],
          ),
          SizedBox(height: 15),
          Expanded(
            child: GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: 0.6,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10),
                shrinkWrap: true,
                itemCount: productList.length,
                padding: EdgeInsets.only(bottom: 100),
                itemBuilder: (context, index) {
                  return content_list(productList[index]);
                }),
          ),
        ],
      ),
    );
  }

  Widget content_list(Map<String, dynamic> listProduct) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(17.0)),
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                // Discount information
                Container(
                  decoration: listProduct['disc'] == ''
                      ? null
                      : BoxDecoration(
                      color: Color(0xFFA4E2FC),
                      borderRadius: BorderRadius.circular(5.0)),
                  child: Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Text(
                      listProduct['disc'],
                      style: subTitle,
                    ),
                  ),
                ),
                // Like product
                GestureDetector(
                  onTap: () {
                    setState(() {
                      listProduct['isLike'] = !listProduct['isLike'];
                    });
                  },
                  child:
                  Container(
                    child:Icon(
                      listProduct['isLike']
                          ? Icons.favorite
                          : Icons.favorite,
                      color: listProduct['isLike'] ? Colors.white : Colors.grey,
                      size: listProduct['isLike'] ? 15 : 13,
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      color: listProduct['isLike'] ? Colors.red:Colors.white,
                      boxShadow: [
                        BoxShadow(color: listProduct['isLike'] ?Colors.red:Colors.white, spreadRadius: 3),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            // Product Image
            InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (_) => ProductDetail(listProduct)));
              },
              child: Container(
                  height: 150,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      image: DecorationImage(
                          image:
                          AssetImage(listProduct['listImage'][0]['image']),
                          fit: BoxFit.cover))),
            ),
            // Product Information
            Text(
              listProduct['name'],
              maxLines: 1,
              style: productTitle,
            ),
            Text(
              Money.fromInt(listProduct['price'], Currency.create('USD', 2))
                  .toString(),
              style: productSubTitle,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RatingBarIndicator(
                  itemBuilder: (context, index) => Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  rating: listProduct['rating'],
                  itemCount: 5,
                  itemSize: 15,
                  direction: Axis.horizontal,
                ),
                Text(
                  ' (' + listProduct['rating'].toString() + ')',
                  style: GoogleFonts.poppins()
                      .copyWith(color: Colors.black, fontSize: 9),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
  Widget category(String title, double shadow, TextStyle style) {
    return Card(
      elevation: shadow,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        child: Row(
          children: <Widget>[
            Text(title, style: style),
          ],
        ),
      ),
    );
  }
}
