import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tradaru_test/views/product_list.dart';
import 'package:tradaru_test/views/uilibs.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int bottomNavBarIndex;
  PageController pageController;

  @override
  void initState() {
    super.initState();
    bottomNavBarIndex = 0;
    pageController = PageController(initialPage: bottomNavBarIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        Container(
          color: baseColor,
        ),
        SafeArea(
            child: Container(
              color: baseColor,
            )),
        PageView(
          controller: pageController,
          onPageChanged: (index) {
            setState(() {
              bottomNavBarIndex = index;
            });
          },
          children: <Widget>[
            ProductList(),
            Center(child: Text("Favorites")),
            Center(child: Text("Tracking")),
            Center(child: Text("Profile")),
          ],
        ),
        navbar(),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            height: 46,
            width: 46,
            margin: EdgeInsets.only(bottom: 42),
            child: Stack(
              children: <Widget>[
                FloatingActionButton(
                  onPressed: () {},
                  elevation: 0,
                  backgroundColor: primaryColor,
                  child: SizedBox(
                    height: 26,
                    width: 26,
                    child: Icon(
                      Icons.shopping_cart,
                      size: 15,
                      color: secondaryColor,
                    ),
                  ),
                ),
                Positioned(
                  right: 0,
                  child: new Container(
                    padding: EdgeInsets.all(1),
                    decoration: new BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(7),
                    ),
                    constraints: BoxConstraints(
                      minWidth: 16,
                      minHeight: 16,
                    ),
                    child: Center(
                      child: new Text(
                        '2',
                        style: new TextStyle(
                          color: Colors.white,
                          fontSize: 8,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        )
      ]),
    );
  }

  Widget navbar() => Align(
    alignment: Alignment.bottomCenter,
    child: ClipPath(
      clipper: BottomNavBarClipper(),
      child: Container(
        height: 70,
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: BottomNavigationBar(
          onTap: (index) {
            setState(() {
              bottomNavBarIndex = index;
              pageController.jumpToPage(index);
            });
          },
          elevation: 0,
          backgroundColor: Colors.transparent,
          selectedItemColor: primaryColor,
          unselectedItemColor: Color(0xFFA9A9A9),
          currentIndex: bottomNavBarIndex,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.home,
                  size: 20,
                ),
                title: Text("")),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.favorite,
                  size: 20,
                ),
                title: Text("")),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.assignment,
                  size: 20,
                ),
                title: Text("")),
            BottomNavigationBarItem(
                icon: Icon(Icons.person,
                  size: 20,
                ),
                title: Text("")),
          ],
        ),
      ),
    ),
  );
}

class BottomNavBarClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();

    path.lineTo(size.width / 2 - 28, 0);
    path.quadraticBezierTo(size.width / 2 - 28, 33, size.width / 2, 33);
    path.quadraticBezierTo(size.width / 2 + 28, 33, size.width / 2 + 28, 0);
    path.lineTo(size.width, 0);
    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
