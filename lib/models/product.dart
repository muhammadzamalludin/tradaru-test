import 'package:flutter/material.dart';

List<Map<String, dynamic>> productList = [
  {
    'name': 'Nike Air Max',
    'desc': 'Lorem Ipsum',
    'price': 24000,
    'rating': 4.3,
    'isLike': true,
    'disc': '30%',
    'listImage': [
      {'image': 'assets/image/airmax_1.jpg'},
      {'image': 'assets/img/airmax_2.jpg'},
      {'image': 'assets/img/airmax_2.jpg'},
    ],
    'listSize': [
      {'name': 'US6'},
      {'name': 'US7'},
      {'name': 'US8'},
      {'name': 'US9'},
    ],
    'listColor': [
      {'name': Colors.red},
      {'name': Colors.yellow},
      {'name': Colors.blue},
    ]
  },
  {
    'name': 'Excee Sneakers',
    'desc': 'Lorem ipsum',
    'price': 26000,
    'rating': 3.8,
    'isLike': false,
    'disc': '',
    'listImage': [
      {'image': 'assets/image/excee_1.png'},
      {'image': 'assets/image/excee_2.png'},
      {'image': 'assets/image/excee_3.png'},
    ],
    'listSize': [
      {'name': 'US6'},
      {'name': 'US7'},
      {'name': 'US8'},
    ],
    'listColor': [
      {'name': Colors.red},
      {'name': Colors.yellow},
      {'name': Colors.blue},
    ]
  },
  {
    'name': 'Airmax motion 2',
    'desc': 'Lorem Ipsum',
    'price': 29000,
    'rating':3.0,
    'isLike': false,
    'disc': '',
    'listImage': [
      {'image': 'assets/image/motion2_1.jpg'},
      {'image': 'assets/image/motion2_2.jpg'},
      {'image': 'assets/image/motion2_3.jpg'},
    ],
    'listSize': [
      {'name': 'US6'},
      {'name': 'US7'},
    ],
    'listColor': [
      {'name': Colors.red},
      {'name': Colors.yellow},
      {'name': Colors.blue},
    ]
  },
  {
    'name': 'Air jordan',
    'desc': 'Lorem ipsum',
    'price': 24000,
    'rating': 3.5,
    'isLike': false,
    'disc': '40%',
    'listImage': [
      {'image': 'assets/image/air_jordan_1.jpg'},
      {'image': 'assets/image/air_jordan_2.jpg'},
      {'image': 'assets/image/air_jordan_3.jpg'},
    ],
    'listSize': [
      {'name': 'US6'},
      {'name': 'US7'},
      {'name': 'US8'},
      {'name': 'US9'},
    ],
    'listColor': [
      {'name': Colors.red},
      {'name': Colors.yellow},
      {'name': Colors.blue},
    ]
  }
];
